import React, { Component } from 'react'
import { connect } from 'react-redux';
import ItemShoe from './ItemShoe';

class ListShoe extends Component {
    renderListShoe = () => {
        return this.props.shoeArr.map((item) => {
            return (
                <ItemShoe data={item}/>);
        })
    };
    render() {
        return (
            <div className='row'>
                {this.renderListShoe()}
            </div>
        );
    }
}
// gọi dự liệu từ store 
let mapStateToProps = (state) => {
    return {
        shoeArr: state.dataReducer.shoeArr,
        //   detail: state.dataState.detail,
        //   cart: state.dataState.cart,
    }
}

export default connect (mapStateToProps)(ListShoe);