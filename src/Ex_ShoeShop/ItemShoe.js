import React, { Component } from 'react'
import { connect } from 'react-redux';
import { ADDTOCART, CHANGEDEATAIL } from './constant/constant';

class ItemShoe extends Component {
    render() {
        let { image, name } = this.props.data
        return (
            <div className="col-3 p-1">
                <div className="card text-left h-100">
                    <img className="card-img-top" src={image} alt />
                    <div className="card-body">
                        <h6 className="card-title">{name}</h6>
                        <div className='d-flex justify-content-between'>
                            <button
                                onClick={() => { this.props.handleaddtoCart(this.props.data); }}
                                className='btn btn-success'>
                                Buy
                            </button>
                            <button
                                onClick={() => { this.props.handleChangeDetail(this.props.data); }}
                                className='btn btn-primary'>Detail</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}



// thay đổi state từ store
let mapDispatchtoProps = (dispatch) => {
    return {
        // hàm thay đổi Detail
        handleChangeDetail: (value) => {
            dispatch({
                type: CHANGEDEATAIL,
                payload: value,
            })
        },

        // hàm nút button buy và tăng số lượng
        handleaddtoCart: (shoe) => {
            dispatch({
                type: ADDTOCART,
                payload: shoe,
            })
        },
    }
}

export default connect(null, mapDispatchtoProps)(ItemShoe);