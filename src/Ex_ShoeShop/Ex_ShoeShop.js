import React, { Component } from 'react'
// import CartShoe, DetailShoe, ListShoe
import CartShoe from './CartShoe';
import DetailShoe from './DetailShoe';
import ListShoe from './ListShoe';

export default class Ex_ShoeShop extends Component {
  render() {
    return (
      <div className='container'>
        {/* cart */}
        <CartShoe/>
        {/* card */}
        <ListShoe/>
        {/* detail */}
        <DetailShoe/>
      </div>
    )
  }
}
