import logo from './logo.svg';
import './App.css';
import Demo_redux from './Demo_redux/Demo_redux';
import Ex_ShoeShop from './Ex_ShoeShop/Ex_ShoeShop';

function App() {
  return (
    <div className="App">
      <Ex_ShoeShop/>
    </div>
  );
}

export default App;
