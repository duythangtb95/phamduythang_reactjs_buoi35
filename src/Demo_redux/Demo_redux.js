import React, { Component } from 'react'
import { connect } from 'react-redux'

class Demo_redux extends Component {
    render() {
        return (
            <div>
                <button
                onClick={ () => { this.props.handleGiamsoluong(5) }}
                className='btn btn-success m-5'>-</button>
                <span>{this.props.soluong}</span>
                <button
                onClick={this.props.handleTangsoluong}
                className='btn btn-success m-5'>+</button>
            </div>
        )
    }
}

// lấy dữ liệu
let mapStateToProps = (state) => {
    return {
        soluong: state.numberReducer.number,
    }
};

// lấY hàm
let mapDispatchToProps = (dispatch) => { 
    return {
        handleTangsoluong: () =>{
            let action = {
                type: "Tang_so_luong"
            };
            dispatch(action);
        },
        handleGiamsoluong: (value) => {
            dispatch({
                type: "Giam_so_luong",
                payload: value,
            })
        }
    }
 };
//  connects
export default connect (mapStateToProps,mapDispatchToProps)(Demo_redux);