import { ADDTOCART, CHANGEDEATAIL, MINIUSNUMBER } from "../../constant/constant";
import { dataShoe } from "../../dataShoe";

let dataState = {
    shoeArr: dataShoe,
    detail: dataShoe[0],
    cart: [],
}

export const dataReducer = (state = dataState, action) => {
    switch (action.type) {
        
        // function cộng thêm số lượng
        case ADDTOCART: {
            let cloneCart = [...state.cart];
            let index = cloneCart.findIndex((item) => {
                return item.id == action.payload.id
            });
            if (index == -1) {
                let itemCart = { ...action.payload, number: 1 };
                cloneCart.push(itemCart)
            } else {
                cloneCart[index].number++;
            };
            return state.cart = { ...state, cart: cloneCart };
        };

        // function trừ số lượng
        case MINIUSNUMBER: {
            let cloneCart = [...state.cart];
            let index = cloneCart.findIndex((item) => {
                return item.id == action.payload.id
            });
            if (action.payload.number > 0) {
                cloneCart[index].number = cloneCart[index].number - 1;
            }
            return state.cart = { ...state, cart: cloneCart };
        };

        // function show thông tin
        case CHANGEDEATAIL: {
            state.detail = action.payload
            return { ...state };
        }
        default: return state;
    }
}