import { combineReducers } from "redux";
import { dataReducer } from "./dataReducer";

export const rootReducer_ShoeShop = combineReducers({
    dataReducer,
})