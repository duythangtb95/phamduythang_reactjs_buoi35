import { type } from "@testing-library/user-event/dist/type";

let numberState = {
    number: 100,
}

export const numberReducer = (state = numberState, action) => {
    switch (action.type) {
        case "Tang_so_luong": {
            state.number++;
            return {...state };
        };
        case "Giam_so_luong": {
            state.number = state.number - action.payload;
            return {...state };
        }
        default: return state;
    }
};